// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TopDownHealthComponent.h"
#include "TopDownCharacterHealthComponent.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWN_API UTopDownCharacterHealthComponent : public UTopDownHealthComponent
{
	GENERATED_BODY()


public:
	
	 void ChangeCurrentHealth(float ChangeValue) override;
	
};
