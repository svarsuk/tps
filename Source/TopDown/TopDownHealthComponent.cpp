// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownHealthComponent.h"

// Sets default values for this component's properties
UTopDownHealthComponent::UTopDownHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTopDownHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTopDownHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTopDownHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTopDownHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTopDownHealthComponent::ChangeCurrentHealth(float ChangeValue)
{

	Health += ChangeValue;
	OnHealthChange.Broadcast(Health, ChangeValue);
	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			OnDead.Broadcast();
		}
	}
	



}

